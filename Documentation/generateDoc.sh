#!/bin/bash

for file in `ls *.tex`; do aspell --mode=tex --lang=en -c $file; done

doxygen
pdflatex dataviewGuide.tex 
makeindex dataviewGuide.idx 
pdflatex dataviewGuide.tex

latex2html dataviewGuide.tex

rm *.aux
rm *.idx
rm *.ilg
rm *.toc
rm *.ind

