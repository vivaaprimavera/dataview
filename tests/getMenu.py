#!/usr/bin/python3

import MySQLdb
import json
import configparser

from dataview import *

config = configparser.ConfigParser()
config.read('databaseconfig')

vis = Visualization(config['Visualization']['server'],config['Visualization']['database'],config['Visualization']['username'],config['Visualization']['password'])
vis.setOutputType() # for setting JSON output
p = json.loads(vis.getMenu())
print(json.dumps(p,indent = 4))
del vis # for closing the database connections
