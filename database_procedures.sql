-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/03
-- date: 2021/05/05 Procedures change
-- ============================================================================

USE dataview2

DELIMITER ;

DROP PROCEDURE IF EXISTS getMenu; --
DROP PROCEDURE IF EXISTS getMenuByDateRange;
DROP PROCEDURE IF EXISTS getMenuParent; --
DROP PROCEDURE IF EXISTS getMenuInstruments; --
DROP PROCEDURE IF EXISTS getInstrumentValues; --
DROP PROCEDURE IF EXISTS getComplexInstrumentValues; --

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/16
-- ============================================================================

DROP PROCEDURE IF EXISTS getMeasureTypes; --

DROP PROCEDURE IF EXISTS putMenu; --
DROP PROCEDURE IF EXISTS putInstrument; --
DROP PROCEDURE IF EXISTS putMeasureType; --
DROP PROCEDURE IF EXISTS putValue; --
DROP PROCEDURE IF EXISTS putComplexValue; --

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/17
-- ============================================================================

DROP PROCEDURE IF EXISTS getInstrumentProperties; --
DROP PROCEDURE IF EXISTS putDescription;
DROP PROCEDURE IF EXISTS putCoordinates;

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/18
-- ============================================================================

DROP PROCEDURE IF EXISTS putProperty;
DROP PROCEDURE IF EXISTS putInstrumentProperty;

-- ============================================================================
-- author: João Silva
-- date: 2021/04/27
-- ============================================================================

DROP PROCEDURE IF EXISTS putTemplate;
DROP PROCEDURE IF EXISTS putUserTemplate;
DROP PROCEDURE IF EXISTS putInstrumentTemplate;

DELIMITER //

-- ============================================================================ 
-- author: João Silva
-- date: 2021/05/05
-- getMenu(idMenu) creates temporary table _Menu with the fields
-- idMenu,name,description,location,parent
-- ============================================================================


CREATE PROCEDURE getMenu(IN _idMenu INT)
BEGIN
	DROP TEMPORARY TABLE IF EXISTS _Menu;
	CREATE TEMPORARY TABLE _Menu AS (SELECT menus.idMenu,name,description,location,parent
		FROM menus 
		INNER JOIN descriptions ON menus.idDescription = descriptions.idDescription
		WHERE parent = _idMenu
		);
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/03
-- getMenuByDateRange(startTime,endTime) creates temporary table _Menu with the fields
-- idMenu,name,description,location,parent
-- if parent != 0 that menu item have a parent menu
-- date: 2021/02/08
-- small corrections and tests
-- date: 2021/04/26 quality control
-- ============================================================================

CREATE PROCEDURE getMenuByDateRange(IN startTime TIMESTAMP,IN endTime TIMESTAMP)
BEGIN
	DROP TEMPORARY TABLE IF EXISTS _Instruments;
	DROP TEMPORARY TABLE IF EXISTS _Menu;
	CREATE TEMPORARY TABLE _Instruments AS (SELECT DISTINCT(idInstrument) as idInstrument
		FROM instrumentValues WHERE datetime >= startTime AND datetime <= endTime );
	INSERT INTO _Instruments (idInstrument) SELECT DISTINCT(idInstrument) as idInstrument
		FROM complexInstrumentValues WHERE datetime >= startTime AND datetime <= endTime;
	CREATE TEMPORARY TABLE _Menu AS (SELECT menus.idMenu,name,description,location,parent
		FROM menus 
		INNER JOIN descriptions ON menus.idDescription = descriptions.idDescription 
		INNER JOIN instruments ON menus.idMenu = instruments.idMenu
		WHERE idInstrument in (SELECT idInstrument FROM _Instruments)
		);
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/03
-- getMenuParent(idMenu) creates temporary table _Menu with the fields
-- idMenu,name,description,location,parent
-- if parent != 0 that menu item have a parent menu
-- date: 2021/02/08
-- small corrections and tests
-- date: 2021/04/26 quality control
-- ============================================================================

CREATE PROCEDURE getMenuParent(IN idMenu INT)
BEGIN
	DROP TEMPORARY TABLE IF EXISTS _Menu;
	CREATE TEMPORARY TABLE _Menu AS (SELECT idMenu,name,description,location,parent
		FROM menus 
		INNER JOIN descriptions ON menus.idDescription = descriptions.idDescription
		WHERE parent = idMenu );
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/03
-- getMenuInstruments(idMenu) creates temporary table _Instruments with the fields
-- idInstrument,instrument,measureType,isComplex
-- date: 2021/02/08
-- small corrections and tests
-- 2021/04/26 quality control, variable name changes
-- ============================================================================

CREATE PROCEDURE getMenuInstruments(IN _idMenu INT)
BEGIN
	DROP TEMPORARY TABLE IF EXISTS _Instruments;
	CREATE TEMPORARY TABLE _Instruments AS (
		SELECT idInstrument,instrument,measureType,isComplex FROM instruments
			INNER JOIN measureTypes ON instruments.idMeasureType = measureTypes.idMeasureType
			WHERE idMenu = _idMenu
		);
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/03
-- getInstrumentValues(idInstrument,startTime,endTime) creates temporary table _Values with the fields
-- datetime,value,error,dataValid
-- date: 2021/02/08
-- small corrections and tests
-- 2021/04/26 quality control, variable name changes
-- ============================================================================

CREATE PROCEDURE getInstrumentValues(IN _idInstrument INT,IN startTime TIMESTAMP,IN endTime TIMESTAMP )
BEGIN
	DROP TEMPORARY TABLE IF EXISTS _Values;
	CREATE TEMPORARY TABLE _VALUES AS (
		SELECT datetime,value,error,dataValid FROM instrumentValues WHERE idInstrument = _idInstrument AND datetime >= startTime AND datetime <= endTime
		);
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/03
-- getComplexInstrumentValues(idInstrument,startTime,endTime) creates temporary table _Values with the fields
-- datetime,value,dataValid
-- value is a JSON
-- date: 2021/02/08
-- small corrections and tests
-- note: due to a lack of JSON type in mariadb in versions prior to 10 the field is a longtext
-- 2021/04/26 quality control, variable name changes
-- ============================================================================

CREATE PROCEDURE getComplexInstrumentValues(IN _idInstrument INT,IN startTime TIMESTAMP,IN endTime TIMESTAMP)
BEGIN
	DROP TEMPORARY TABLE IF EXISTS _Values;
	CREATE TEMPORARY TABLE _Values AS (
		SELECT datetime,value,dataValid FROM complexInstrumentValues WHERE idInstrument = _idInstrument AND datetime >= startTime AND datetime <= endTime
		);
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/16
-- getMeasureTypes creates temporary table _MeasureTypes with the field measureType
-- date: 2021/04/26 quality control
-- date: 2021/05/05 change of output
-- ============================================================================

CREATE PROCEDURE getMeasureTypes()
BEGIN
	DROP TEMPORARY TABLE IF EXISTS _MeasureTypes;
	CREATE TEMPORARY TABLE _MeasureTypes AS (
		SELECT idMeasureType,measureType FROM measureTypes
		);
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/17
-- getInstumentProperties(IN idInstrument INT) creates temporary table _Properties with the field property
-- date: 2021/04/26 quality control, variable name changes
-- ============================================================================

CREATE PROCEDURE getInstrumentProperties(IN _idInstrument INT)
BEGIN
	DROP TEMPORARY TABLE IF EXISTS _Properties;
	CREATE TEMPORARY TABLE _Properties AS (
		SELECT property FROM Properties INNER JOIN instrumentProperties ON
			instrumentProperties.idProperty = Properties.idProperty
			WHERE idInstrument = _idInstrument
		);
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/17
-- putDescription - NOT to be used directly
-- date: 2021/04/23 quality control, variable name changes
-- date: 2021/04/30 Bug fix
-- ============================================================================

CREATE PROCEDURE putDescription(IN _description VARCHAR(1024),OUT retval INT)
BEGIN
	SET @id = (SELECT COUNT(*) FROM descriptions WHERE description = _description);
	IF ( @id > 0 ) THEN
		SET retval = (SELECT idDescription FROM descriptions WHERE description = _description);
	ELSE
		START TRANSACTION;
			INSERT INTO descriptions (description) VALUES (_description);
		COMMIT;
		SET retval = LAST_INSERT_ID();
	END IF;
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/17
-- putCoordinates - NOT to be used directly
-- date: 2021/04/23 quality control, variable name changes
-- ============================================================================

CREATE PROCEDURE putCoordinates(IN _latitude FLOAT,IN _longitude FLOAT,IN _altitude FLOAT, OUT retval INT)
BEGIN
	SET @id = (SELECT COUNT(*) FROM coordinates WHERE latitude = _latitude AND longitude = _longitude AND altitude = _altitude );
	IF ( @id > 0 ) THEN
		SET retval = (SELECT idCoordinate FROM coordinates WHERE latitude = _latitude AND longitude = _longitude AND altitude = _altitude);
	ELSE
		START TRANSACTION;
			INSERT INTO coordinates (latitude,longitude,altitude) VALUES (_latitude,_longitude,_altitude);
		COMMIT;
		SET retval = LAST_INSERT_ID();
	END IF;
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/17
-- putMenu(IN name VARCHAR(255),IN description VARCHAR(1024),IN location VARCHAR(255),IN latitude FLOAT, IN longitude FLOAT,IN altitude FLOAT,IN parent INT, OUT retval INT) retval is the new idMenu
-- date: 2021/04/23 quality control, variable name changes
-- ============================================================================

CREATE PROCEDURE putMenu(IN _name VARCHAR(255),IN _description VARCHAR(1024),IN _location VARCHAR(255),IN _latitude FLOAT, IN _longitude FLOAT,IN _altitude FLOAT,IN _parent INT, OUT retval INT)
BEGIN
	SET @id = (SELECT COUNT(*) FROM menus WHERE name = _name AND parent = _parent);
	IF ( @id > 0 ) THEN
		SET retval = (SELECT idMenu FROM menus WHERE name = _name AND parent = _parent);
	ELSE
		CALL putDescription(_description,@idDescription);
		CALL putCoordinates(_latitude,_longitude,_altitude,@idCoordinate);
		START TRANSACTION;
			INSERT INTO menus (name,idDescription,location,idCoordinate,parent) VALUES (_name,@idDescription,_location,@idCoordinate,_parent);
		COMMIT;
		SET retval = LAST_INSERT_ID();
	END IF;
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/18
-- putMeasureType(IN easureType VARCHAR(255), OUT retval INT) retval is the new idMeasureTYpe
-- date: 2021/04/26 variable name changes
-- ============================================================================

CREATE PROCEDURE putMeasureType(IN _measureType VARCHAR(255), OUT retval INT)
BEGIN
	SET @id = (SELECT COUNT(*) FROM measureTypes WHERE measureType = _measureType);
	IF ( @id > 0 ) THEN
		SET retval = (SELECT idMeasureType FROM measureTypes WHERE measureType = _measureType);
	ELSE
		START TRANSACTION;
			INSERT INTO measureTypes (measureType) VALUES (_measureType);
		COMMIT;
		SET retval = LAST_INSERT_ID();
	END IF;
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/18
-- putInstrument(IN instrument VARCHAR(255),IN idMenu INT,IN idMeasureType INT,IN isComplex TINYINT, OUT retval INT) retval is the new idInstrument
-- date: 2021/04/23 quality control
-- date: 2021/04/26, variable name changes
-- ============================================================================

CREATE PROCEDURE putInstrument(IN _instrument VARCHAR(255),IN _idMenu INT,IN _idMeasureType INT,IN _isComplex TINYINT, OUT retval INT)
BEGIN
	SET @id = (SELECT COUNT(*) FROM instruments WHERE instrument = _instrument AND idMenu = _idMenu);
	IF ( @id > 0 ) THEN
		SET retval = (SELECT idInstrument FROM instruments WHERE instrument = _instrument AND idMenu = _idMenu);
	ELSE
		START TRANSACTION;
			INSERT INTO instruments (instrument,idMenu,idMeasureType,isComplex) VALUES (_instrument,_idMenu,_idMeasureTYpe,_isComplex);
		COMMIT;
		SET retval = LAST_INSERT_ID();
	END IF;
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/18
-- putValue(IN idInstrument INT,IN datetime TIMESTAMP, IN value FLOAT, IN error FLOAT)
-- date: 2021/04/26 variable name changes
-- ============================================================================

CREATE PROCEDURE putValue(IN _idInstrument INT,IN _datetime TIMESTAMP, IN _value FLOAT, IN _error FLOAT)
BEGIN
	START TRANSACTION;
		INSERT INTO instrumentValues (idInstrument,datetime,value,error) VALUES (dInstrument,atetime,alue,rror);
	COMMIT;
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/18
-- putComplexValue(IN dInstrument INT,IN atetime TIMESTAMP, IN alue LONGTEXT)
-- value is a LONGTEXT due to a lack of support for JSON in this version of mariadb
-- date: 2021/04/26 variable name changes
-- ============================================================================

CREATE PROCEDURE putComplexValue(IN _idInstrument INT,IN _datetime TIMESTAMP, IN _value LONGTEXT)
BEGIN
	START TRANSACTION;
		INSERT INTO complexInstrumentValues (idInstrument,datetime,value) VALUES (_idInstrument,_datetime,_value);
	COMMIT;
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/18
-- putProperty(IN _property LONGTEXT,OUT retval INT) - not to be used directly
-- not to be used directly
-- date: 2021/04/26 variable name changes
-- date: 2021/04/26 bug fix
-- ============================================================================

CREATE PROCEDURE putProperty(IN _property LONGTEXT,OUT retval INT)
BEGIN
	SET @id = (SELECT COUNT(*) FROM Properties WHERE property = _property);
	IF ( @id > 0 ) THEN
		SET retval = (SELECT idProperty FROM Properties WHERE property = _property);
	ELSE
		START TRANSACTION;
			INSERT INTO Properties (property) VALUES (_property);
		COMMIT;
		SET retval = LAST_INSERT_ID();
	END IF;
END//

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/18
-- putInstrumentProperty(IN idInstrument INT,IN property LONGTEXT)
-- date: 2021/02/26
-- ============================================================================

CREATE PROCEDURE putInstrumentProperty(IN _idInstrument INT,IN _property LONGTEXT)
BEGIN
	CALL putProperty(_property,@idProperty);
	START TRANSACTION;
		INSERT INTO instrumentProperties (idProperty,idInstrument) VALUES (@idProperty,_idInstrument);
	COMMIT;	
END//

-- ============================================================================
-- author: João Silva
-- date: 2021/04/27
-- putTemplate(IN _templateName VARCHAR(255),IN _template LONGTEXT, OUT retval INT)
-- not to be use directly
-- ============================================================================

CREATE PROCEDURE putTemplate(IN _templateName VARCHAR(255),IN _template LONGTEXT, OUT retval INT)
BEGIN
	SET @id = (SELECT COUNT(*) FROM templates WHERE templateName = _templateName AND template = _template);
	IF ( @id > 0 ) THEN
		SET retval = (SELECT idTemplate FROM templates WHERE templateName = _templateName AND template = _template);
	ELSE
		START TRANSACTION;
			INSERT INTO templates (templateName,template) VALUES (_templateName,_template);
		COMMIT;
		SET retval = LAST_INSERT_ID();
	END IF;
END//

-- ============================================================================
-- author: João Silva
-- date: 2021/04/27
-- putInstrumentTemplate(IN _idInstrument INT,IN _templateName VARCHAR(255),IN _template LONGTEXT)
-- ============================================================================

CREATE PROCEDURE putInstrumentTemplate(IN _idInstrument INT,IN _templateName VARCHAR(255),IN _template LONGTEXT)
BEGIN
	CALL putTemplate(_templateName,_template,@idTemplate);
	START TRANSACTION;
		INSERT INTO instrumentTemplates (idInstrument,idTemplate) VALUES (_idInstrument,@idTemplate);
	COMMIT;
END//

-- ============================================================================
-- author: João Silva
-- date: 2021/04/27
-- putUserTemplate(IN _idUser INT,IN _templateName VARCHAR(255),IN _template LONGTEXT)
-- ============================================================================

CREATE PROCEDURE putUserTemplate(IN _idUser INT,IN _templateName VARCHAR(255),IN _template LONGTEXT)
BEGIN
	CALL putTemplate(_templateName,_template,@idTemplate);
	START TRANSACTION;
		INSERT INTO userTemplates (idUser,idTemplate) VALUES (_idUser,@idTemplate);
	COMMIT;
END//

DELIMITER ;
