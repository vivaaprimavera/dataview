#!/bin/bash

###############################################################################
# Author: João Silva
# date: 2021/02/22
###############################################################################

USER_FOR_ADMINISTRATION="procAdmin"
USER_FOR_VISUALIZATION="visAdmin"
USER_FOR_DATA_INSERTION="dataMonkey"

PASSWORD_FOR_ADMINISTRATION_USER="somethingveryesoteric"
PASSWORD_FOR_VISUALIZATION_USER="interpass@webserver"
PASSWORD_FOR_DATA_USER="InputValues..."

DATABASENAME="dataview2"

###############################################################################
# User creation
###############################################################################

echo "USE $DATABASENAME"
echo "CREATE USER '$USER_FOR_ADMINISTRATION'@'localhost' IDENTIFIED BY '$PASSWORD_FOR_ADMINISTRATION_USER';"
echo "CREATE USER '$USER_FOR_VISUALIZATION'@'localhost' IDENTIFIED BY '$PASSWORD_FOR_VISUALIZATION_USER';"
echo "CREATE USER '$USER_FOR_DATA_INSERTION'@'localhost' IDENTIFIED BY '$PASSWORD_FOR_DATA_USER';"

###############################################################################
# Administration procedures
###############################################################################

echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.getMeasureTypes TO '$USER_FOR_ADMINISTRATION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.putMenu TO '$USER_FOR_ADMINISTRATION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.putInstrument TO '$USER_FOR_ADMINISTRATION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.putMeasureType TO '$USER_FOR_ADMINISTRATION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.putInstrumentProperty TO '$USER_FOR_ADMINISTRATION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.putInstrumentTemplate TO '$USER_FOR_ADMINISTRATION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.getMenu TO '$USER_FOR_ADMINISTRATION'@'localhost';"

###############################################################################
# Visualization Procedures
###############################################################################

echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.getMenu TO '$USER_FOR_VISUALIZATION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.getMenuByDateRange TO '$USER_FOR_VISUALIZATION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.getMenuParent TO '$USER_FOR_VISUALIZATION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.getMenuInstruments TO '$USER_FOR_VISUALIZATION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.getInstrumentValues TO '$USER_FOR_VISUALIZATION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.getComplexInstrumentValues TO '$USER_FOR_VISUALIZATION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.getInstrumentProperties TO '$USER_FOR_VISUALIZATION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.putUserTemplate TO '$USER_FOR_VISUALIZATION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.getMeasureTypes TO '$USER_FOR_VISUALIZATION'@'localhost';"
###############################################################################
# Data insertion Procedures
###############################################################################

echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.putValue TO '$USER_FOR_DATA_INSERTION'@'localhost';"
echo "GRANT EXECUTE ON PROCEDURE $DATABASENAME.putComplexValue TO '$USER_FOR_DATA_INSERTION'@'localhost';"
