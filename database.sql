-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/01
-- ============================================================================

USE dataview2

CREATE TABLE IF NOT EXISTS descriptions (
	idDescription INT AUTO_INCREMENT PRIMARY KEY,
	description VARCHAR(1024)
	) DEFAULT CHARSET=UTF8 ;

CREATE TABLE IF NOT EXISTS coordinates (
	idCoordinate INT AUTO_INCREMENT PRIMARY KEY,
	latitude FLOAT DEFAULT 0,
	longitude FLOAT DEFAULT 0,
	altitude FLOAT DEFAULT 0
	) DEFAULT CHARSET=UTF8 ;

CREATE TABLE IF NOT EXISTS menus (
	idMenu INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(255),
	idDescription INT DEFAULT 0,
	location VARCHAR(255),
	idCoordinate INT DEFAULT 0,
	parent INT DEFAULT 0,
	FOREIGN KEY (idDescription) REFERENCES descriptions (idDescription),
	FOREIGN KEY (idCoordinate) REFERENCES coordinates (idCoordinate)
	) DEFAULT CHARSET=UTF8 ;

CREATE TABLE IF NOT EXISTS measureTypes (
	idMeasureType INT AUTO_INCREMENT PRIMARY KEY,
	measureType VARCHAR(255)
	) DEFAULT CHARSET=UTF8 ;

CREATE TABLE IF NOT EXISTS instruments (
	idInstrument INT AUTO_INCREMENT PRIMARY KEY,
	instrument VARCHAR(255),
	idMenu INT NOT NULL,
	idMeasureType INT NOT NULL,
	isComplex TINYINT DEFAULT 0,
	FOREIGN KEY (idMenu) REFERENCES menus (idMenu),
	FOREIGN KEY (idMeasureType) REFERENCES measureTypes (idMeasureType)	
	) DEFAULT CHARSET=UTF8 ;

CREATE TABLE IF NOT EXISTS instrumentValues (
	datetime TIMESTAMP NOT NULL,
	idInstrument INT NOT NULL,
	value FLOAT,
	error FLOAT,
	dataValid TINYINT DEFAULT '1',
	PRIMARY KEY (datetime,idInstrument),
	FOREIGN KEY (idInstrument) REFERENCES instruments (idInstrument)
	) DEFAULT CHARSET=UTF8 ;

CREATE TABLE IF NOT EXISTS complexInstrumentValues (
	datetime TIMESTAMP NOT NULL,
	idInstrument INT NOT NULL,
	value LONGTEXT,
	dataValid TINYINT DEFAULT '1',
	PRIMARY KEY (datetime,idInstrument),
	FOREIGN KEY (idInstrument) REFERENCES instruments (idInstrument)
	) DEFAULT CHARSET=UTF8 ;

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/02
-- ============================================================================

CREATE TABLE IF NOT EXISTS users (
	idUser INT AUTO_INCREMENT PRIMARY KEY,
	username VARCHAR(255) NOT NULL,
	pass VARCHAR(255)
	) DEFAULT CHARSET=UTF8 ;

CREATE TABLE IF NOT EXISTS dns (
	idDn INT AUTO_INCREMENT PRIMARY KEY,
	dn VARCHAR(512)
	) DEFAULT CHARSET=UTF8 ;

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/02
-- date: 2021/04/26 Foreign key definitions
-- ============================================================================

CREATE TABLE IF NOT EXISTS userDn (
	idDn INT,
	idUser INT,
	PRIMARY KEY (idDn,idUser),
	FOREIGN KEY (idDn) REFERENCES dns (idDn),
	FOREIGN KEY (idUser) REFERENCES users (idUser)
	) DEFAULT CHARSET=UTF8 ;

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/02
-- ============================================================================

CREATE TABLE IF NOT EXISTS templates (
	idTemplate INT AUTO_INCREMENT PRIMARY KEY,
	templateName VARCHAR(255),	
	template LONGTEXT
	) DEFAULT CHARSET=UTF8 ;

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/17
-- date: 2021/04/26 Foreign key definitions
-- ============================================================================

CREATE TABLE IF NOT EXISTS userTemplates (
	idUser INT,
	idTemplate INT,
	PRIMARY KEY (idUser,idTemplate),
	FOREIGN KEY (idUser) REFERENCES users (idUser),
	FOREIGN	KEY (idTemplate) REFERENCES templates (idTemplate)
	) DEFAULT CHARSET=UTF8 ;

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/17
-- ============================================================================

CREATE TABLE IF NOT EXISTS Properties (
	idProperty INT AUTO_INCREMENT PRIMARY KEY,	
	property LONGTEXT
	) DEFAULT CHARSET=UTF8 ;

-- ============================================================================ 
-- author: João Silva
-- date: 2021/02/17
-- date: 2021/04/26 Foreign key definitions
-- ============================================================================

CREATE TABLE IF NOT EXISTS instrumentProperties (
	idProperty INT,
	idInstrument INT,
	PRIMARY KEY (idProperty,idInstrument),
	FOREIGN KEY (idProperty) REFERENCES Properties (idProperty),
	FOREIGN KEY (idInstrument) REFERENCES instruments (idInstrument)
	) DEFAULT CHARSET=UTF8 ;

-- ============================================================================ 
-- author: João Silva
-- date: 2021/04/26 
-- ============================================================================

CREATE TABLE IF NOT EXISTS instrumentTemplates (
	idInstrument INT,
	idTemplate INT,
	PRIMARY KEY (idInstrument,idTemplate),
	FOREIGN KEY (idInstrument) REFERENCES instruments (idInstrument),
	FOREIGN KEY (idTemplate) REFERENCES templates (idTemplate)
	)
