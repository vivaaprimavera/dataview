#!/usr/bin/python3
# -*- coding: UTF-8 -*-
# author: João Silva
# date: Date 2021/05/05
# date: Date 2021/05/06
# date: Date 2021/05/11

"""! @brief Defines the classes for Dataview. """

##
# @file dataview.py
#


import MySQLdb
import json

class DataviewRoot:

	"""! The base class for all the Dataview classes

	"""
	
	def __init__(self,databaseServer,databaseName,databaseUser,databasePassword):
		"""! The initializer shared by all the classes
		@param databaseServer The host where the database is located
		@param databaseName The name of the database
		@param databaseUser The user of the database
		@param databasePassword The database password
		"""
		self.db = False
		self.outputType = False	
		try:
			self.db = MySQLdb.connect(databaseServer,databaseUser,databasePassword,databaseName)
		except Exception:
			raise Exception
	
	def setOutputType(self,outputType = 'JSON'):
		"""! Defines the output type for all the database access functions, the default output type set by this function is JSON, in the future is possible to have more output types
		"""
		self.outputType = outputType

	def unsetOutputType(self):
		"""! Sets outputType to false

		"""
		self.outputType = False

	def __del__(self):
		"""! The class destructor, closes the database connection

		"""
		if self.db:
			self.db.close()	

class Administration(DataviewRoot):

	def __init__(self,databaseServer,databaseName,databaseUser,databasePassword):
		"""! The class initializer, inherits from the class DataviewRoot
		@param databaseServer The host where the database is located
		@param databaseName The name of the database
		@param databaseUser The user of the database
		@param databasePassword The database password
		"""
		super().__init__(databaseServer,databaseName,databaseUser,databasePassword)
	
	def putMenu(self,name,description,location,latitude,longitude,altitude,parent = 0):
		"""! putMenu creates a new Menu in the database
		@params name The name of the menu
		@params description A brief description for the Menu
		@params location A brief description of the Menu location
		@params latitude The latitude of the Menu, used for Instruments georeferencing
		@params longitude The longitude of the Menu, used for Instruments georeferencing
		@params altitude The altitude of the Menu, used for Instruments georeferencing
		@params parent The parent Menu, used for creating sub-menus
		@returns If outputType is set to JSON a JSON containing the idMenu of the created menu, otherwise the id of the menu
		"""
		cursor = self.db.cursor()
		cursor.callproc('putMenu',(name,description,location,latitude,longitude,altitude,parent,idMenu))
		cursor.close()
		if self.outputType == 'JSON':
			retval = [{'idMenu' : idMenu}]
			return json.dumps(retval)
		else:
			return idMenu

	def putMeasureType(self,measureType):
		"""! putMeasureType put a new measure type in the database
		@params measureType The measure type
		@returns if outputType is set to JSON the output is a JSON containing the idMeasureType otherwise is the idMeasureType
		"""
		cursor = self.db.cursor()
		cursor.callproc('putMeasureType',(measureType,idMeasureType))
		cursor.close()
		if self.outputType == 'JSON':
			retval = [{'idMeasureType' : idMeasureType}]
			return json.dumps(retval)
		else:
			return idMeasureType

	def putInstrument(self,instrument,idMenu,idMeasureType,isComplex=0):
		"""! putInstrument Creates a new instrument in the database
		@params instrument The name for the instrument
		@params idMenu The id of the Menu under which the Instrument will be placed
		@params idMeasureType the id of the MeasureType for that Instrument
		@params isComplex if set to 0 the instrument only stores values, if 1 it can store JSON files with the Instrument data
		@returns if outputType is set to JSON the output is a JSON with the idInstrument otherwse is the idInstrument
		"""
		cursor = self.db.cursor()
		cursor.callproc('putInstrument',(instrument,idMenu,idMeasureType,isComplex,idInstrument))
		cursor.close()
		if self.outputType == 'JSON':
			retval = [{'idInstrument' : idInstrument}]
			return json.dumps(retval)
		else:
			return idInstrument

	def putInstrumentProperty(self,idInstrument,_property):
		"""! putInstrumentProperty Stores in the database properties for the instrument
		@params idInstrument the id of the Instrument
		@params _property the property or properties of the instrument. Must be a JSON
		"""
		cursor = self.db.cursor()
		cursor.callproc('putInstrumentProperty',(idInstrument,_property))
		cursor.close()

	def putInstrumentTemplate(self,idInstrument,templateName,template):
		"""! putInstrumentTemplate Stores in the database the visualization template for that instrument
		@params idInstrument the id of the Instrument
		@params templateName the name for the template
		@params template the template for the visualization. Must be a JSON
		"""
		cursor = self.db.cursor()
		cursor.callproc('putInstrumentTemplate',(idInstrument,templateName,template))
		cursor.close()

	def getMeasureTypes(self):
		"""! getMeasureTypes lists the measure types stored in the database 
		@returns if outputType is set to JSON returns a JSON containing pairs of idMeasureType, measureType otherwise it returns the result of a cursor.fetchall for that querie
		"""
		cursor = self.db.cursor()
		cursor.callproc('getMeasureTypes')
		sql = 'SELECT idMeasureType,measureType FROM _MeasureTypes'
		cursor.execute(sql)
		res = cursor.fetchall()
		if self.outputType == 'JSON':
			r = []
			for i in range(len(res)):
				b = {}
				for j,value in enumerate(res[i]):
					b[cursor.description[j][0]] = value
				r.append(b)
		cursor.close()
		if self.outputType == 'JSON':
			return json.dumps(r)
		else:
			return res

class Visualization(DataviewRoot):

	def __init__(self,databaseServer,databaseName,databaseUser,databasePassword):
		"""! The class initializer, inherits from the class DataviewRoot
		@param databaseServer The host where the database is located
		@param databaseName The name of the database
		@param databaseUser The user of the database
		@param databasePassword The database password
		"""
		super().__init__(databaseServer,databaseName,databaseUser,databasePassword)
	
	def getMenu(self,parent=0):
		cursor = self.db.cursor()
		cursor.callproc('getMenu',(parent,))
		sql = "SELECT idMenu,name,description,location,parent FROM _Menu"				
		cursor.execute(sql)
		res = cursor.fetchall()
		if self.outputType == 'JSON':
			r = []
			for i in range(len(res)):
				b = {}
				for j,value in enumerate(res[i]):
					b[cursor.description[j][0]] = value
				submenu = json.loads(self.getMenu(b['idMenu']))
				if submenu:
					b['menus'] = submenu
				instruments = json.loads(self.getMenuInstruments(b['idMenu']))
				if instruments:
					b['instruments'] = instruments
				r.append(b)						
		cursor.close()
		if self.outputType == 'JSON':
			return json.dumps(r)
		else:
			return res

	def getMenuByDateRange(self,startDate,endDate):
		cursor = self.db.cursor()
		cursor.callproc('getMenuByDateRange',(startDate,endDate))
		sql = "SELECT idMenu,name,description,location,parent FROM _Menu"		
		cursor.execute(sql)
		res = cursor.fetchall()
		cursor.close()
		return res
	
	def getMenuParent(self,idMenu):
		cursor = self.db.cursor()
		cursor.callproc('getMenuParent',(idMenu))
		sql = "SELECT idMenu,name,description,location,parent FROM _Menu"		
		cursor.execute(sql)
		res = cursor.fetchall()
		cursor.close()
		return res

	def getMenuInstruments(self,idMenu):
		cursor = self.db.cursor()
		cursor.callproc('getMenuInstruments',(idMenu,))
		sql = 'SELECT idInstrument,instrument,measureType,isComplex FROM _Instruments'
		cursor.execute(sql)
		res = cursor.fetchall()
		if self.outputType == 'JSON':
			r = []
			for i in range(len(res)):
				b = {}
				for j,value in enumerate(res[i]):
					b[cursor.description[j][0]] = value
				r.append(b)
		cursor.close()
		if self.outputType == 'JSON':
			return json.dumps(r)
		else:
			return res

	def getInstrumentValues(self,idInstrument,startTime,endTime):
		cursor = self.db.cursor()
		cursor.callproc('getInstrumentValues',(idInstrument,startTime,endTime))
		sql = 'SELECT datetime,value,error,dataValid FROM _Values'
		cursor.execute(sql)
		res = cursor.fetchall()
		if self.outputType == 'JSON':
			r = []
			for i in range(len(res)):
				b = {}
				for j,value in enumerate(res[i]):
					b[cursor.description[j][0]] = value
				r.append(b)
		cursor.close()
		if self.outputType == 'JSON':
			return json.dumps(r)
		else:
			return res
	
	def getComplexInstrumentValues(self,idInstrument,startTime,endTime):
		cursor = self.db.cursor()
		cursor.callproc('getComplexInstrumentValues',(idInstrument,startTime,endTime))
		sql = 'SELECT datetime,value,dataValid FROM _Values'
		cursor.execute(sql)
		res = cursor.fetchall()
		if self.outputType == 'JSON':
			r = []
			for i in range(len(res)):
				b = {}
				for j,value in enumerate(res[i]):
					b[cursor.description[j][0]] = value
				r.append(b)
		cursor.close()
		if self.outputType == 'JSON':
			return json.dumps(r)
		else:
			return res

	def getMeasureTypes(self):
		cursor = self.db.cursor()
		cursor.callproc('getMeasureTypes')
		sql = 'SELECT idMeasureType,measureType FROM _MeasureTypes'
		cursor.execute(sql)
		res = cursor.fetchall()
		if self.outputType == 'JSON':
			r = []
			for i in range(len(res)):
				b = {}
				for j,value in enumerate(res[i]):
					b[cursor.description[j][0]] = value
				r.append(b)
		cursor.close()
		if self.outputType == 'JSON':
			return json.dumps(r)
		else:
			return res

	def getInstrumentProperties(self,idInstrument):
		cursor = self.db.cursor()
		cursor.callproc('getInstrumentProperties',(idInstrument))
		sql = 'SELECT property FROM _Properties'
		cursor.execute(sql)
		res = cursor.fetchall()
		if self.outputType == 'JSON':
			r = []
			for i in range(len(res)):
				b = {}
				for j,value in enumerate(res[i]):
					b[cursor.description[j][0]] = value
				r.append(b)
		cursor.close()
		if self.outputType == 'JSON':
			return json.dumps(r)
		else:
			return res

	def putUserTemplate(self,idUser,templateName,template):
		cursor = self.db.cursor()
		cursor.callproc('putUserTemplate',(idUser,templateName,template))
		cursor.close()

class DataInput(DataviewRoot):

	def __init__(self,databaseServer,databaseName,databaseUser,databasePassword):
		"""! The class initializer, inherits from the class DataviewRoot
		@param databaseServer The host where the database is located
		@param databaseName The name of the database
		@param databaseUser The user of the database
		@param databasePassword The database password
		"""
		super().__init__(databaseServer,databaseName,databaseUser,databasePassword)

	def putValue(self,idInstrument,datetime,value,error):
		cursor = self.db.cursor()
		cursor.callproc('putValue',(idInstrument,datetime,value,error))
		cursor.close()

	def putComplexValue(self,idInstrument,datetime,value):
		cursor = self.db.cursor()
		cursor.callproc('putComplexValue',(idInstrument,datetime,value))
		cursor.close()
